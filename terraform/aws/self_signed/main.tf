resource "tls_private_key" "rsa_self_signed" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

terraform {
  required_providers {
    tls = {
      source = "hashicorp/tls"
      version = "3.4.0"
    }
  }
}

resource "tls_self_signed_cert" "certificate_self_signed" {
  private_key_pem = tls_private_key.rsa_self_signed.private_key_pem

  subject {
    common_name  = "${local.beanstalk_prefix}.${var.region}.elasticbeanstalk.com"
    organization = var.name
  }

  validity_period_hours = 720

  allowed_uses = [
    "any_extended",
    "cert_signing",
    "client_auth",
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}

resource "aws_acm_certificate" "certificate_self_signed" {
  certificate_body = tls_self_signed_cert.certificate_self_signed.cert_pem
  private_key      = tls_private_key.rsa_self_signed.private_key_pem
}

data "aws_lb_listener" "listener80_self_signed" {
  load_balancer_arn = var.aws_elastic_beanstalk_environment.load_balancers[0]
  port              = 80
}

resource "aws_lb_listener_rule" "redirect_http_to_https_self_signed" {
  listener_arn = data.aws_lb_listener.listener80_self_signed.arn

  action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }

  condition {
    host_header {
      values = ["${local.beanstalk_prefix}.${var.region}.elasticbeanstalk.com"]
    }
  }
}

resource "random_integer" "random" {
  min = 1
  max = 500000
}

locals {
  beanstalk_prefix = "${var.name}-${random_integer.random.result}"
}